> Ce projet a été présenté lors d'un wespeakcloud. Retrouvez l'[enregistrement sur youtube](https://www.youtube.com/watch?v=a57npdDAA9Q).




# Déploiement
## opérateur
Déployer l'opérateur :

`kubectl apply -f 1_all-in-one.yaml`

Après avoir installé l'opérateur on peut vérifier les logs :
`kubectl -n elastic-system logs -f statefulset.apps/elastic-operator`

## PersistantVolumeClaim
Création d'une classe de stockage. 
`kubectl apply -f 2_es-wespeakcloud_storageclass.yaml`

## Generation des secrets
Se déplacer dans le répertoire `secrets`.
Créez dans le répertoire CREDS les fichiers suivant :
AWS_ACCESS_KEY_SECRET
AWS_ACCESS_KEY_ID

Y placer dedans la valeur correspondante.
ATTENTION pas de retour à la ligne, ni d'espace en trop. Seulement les valeurs.
Ensuite générer le manifest des secrets : 
`sh generate-secrets-manifest.sh`
Créer les secrets :
`kubectl apply -f secrets.yaml`

## ElasticSearch
Après le lancement le déploiement du noeud de bootstrap ElasticSearch, on peut déployer la nouvelle configuration avec les multi master et multi data nodes.
L'opérateur va mettre à jour le cluster en rolling update, il va d'abord déployer un nouveau master et un nouveau data node, puis retirer les nodesets 'default' au fur et à mesure que les nouveaux nodesets seront activés.

Déploiement des noeuds de bootstrap :
`kubectl apply -f 3_bootstrap_elasticsearch.yaml`

Attendre que l'état du cluster soit 'READY'.

Déploiement de la nouvelle configuration du cluster :
`kubectl apply -f 4_elasticsearch_multi-roles.yaml`

## Kibana

Une fois déployé, rediriger le port du port sur votre machine : kubectl port-forward service/kb-wespeakcloud-kb-http 5601
Volontairement pas de load-balancer ici, à vous d'adapter en fonction de votre environment.
`kubectl apply -f 5_kibana.yaml`

## Filebeat
Filebeat collecte les logs systèmes et applicatifs et les envoies dans le cluster ElasticSearch.
`kubectl apply -f 6_filebeat.yaml`

## Kube state metrics
Kube-state-metrics permet d'exposer pleins de métriques K8S.
`kubectl apply -f kube-state-metrics`

## Metricbeat
Metricbeat collecte les métriques systèmes et applicatives.
`kubectl apply -f 7_metricbeat-kubernetes.yaml`


# Visualisations logs & metriques
Bind du port Kibana sur localhost:
`kubectl port-forward service/kb-wespeakcloud-kb-http 5601`

On accède à Kibana en HTTPS 
https://localhost:5601

Login : elastic
Pour récupérer le mot de passe de l'utilisateur elastic : 
`kubectl get secret es-wespeakcloud-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo`


# Attention

Ces sources vous permettent de tester la mise en place d'un cluster EKS, avec l'opérateur ECK ainsi que les agents Filebeat et MetricBeat. Des choix ont été faits seulement dans le cadre du POC, et notamment du WespeakCloud. N'utilisez en aucun cas ces sources telles quelles sans comprendre et adapter à vos besoins, ajouter vos propres intégrations.
Le déploiement d'un cluster EKS n'est pas gratuit, si vous utilisez un compte personnel veillez à éteindre le cluster après avoir réalisé vos tests.

