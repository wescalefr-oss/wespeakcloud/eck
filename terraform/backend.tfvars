region         = "eu-west-1"
bucket         = "wespeakcloud-eks-terraform-state"
dynamodb_table = "wespeakcloud-terraform-state-lock"
key            = "dev/terraform.tfstate"
