# Create an AWS IAM policy to allow MetricBeat collect metrics from AWS services

resource "aws_iam_policy" "metricbeat-eks" {
    policy = file("policies/metricbeat-iam-policy.json")
}
