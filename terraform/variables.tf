
variable "region" {
  default = "eu-west-1"
  type    = string
}

variable "cluster-name" {
  default = "wespeakcloud"
  type    = string
}

variable "env" {
  default = "integration"
  type    = string
}
