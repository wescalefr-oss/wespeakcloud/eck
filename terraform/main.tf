resource "aws_iam_role" "eks-wescale" {
  name = "wescale-eks-cluster-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "kube-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-wescale.name
}

resource "aws_iam_role_policy_attachment" "kube-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks-wescale.name
}

resource "aws_security_group" "sg-eks-wescale" {
  name        = "eks-wescale"
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.wespeakcloud.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-eks-wespeakcloud"
  }
}

resource "aws_security_group_rule" "wespeakcloud-cluster-ingress-workstation-https" {
  cidr_blocks       = [local.workstation-external-cidr]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.sg-eks-wescale.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_eks_cluster" "kube_cluster" {
  name     = var.cluster-name
  role_arn = aws_iam_role.eks-wescale.arn
  vpc_config {
    security_group_ids = [aws_security_group.sg-eks-wescale.id]
    subnet_ids         = aws_subnet.wespeakcloud[*].id
  }
  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.kube-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.kube-AmazonEKSClusterPolicy,
  ]
}

output "endpoint" {
  value = aws_eks_cluster.kube_cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.kube_cluster.certificate_authority.0.data
}


resource "aws_iam_role" "integ_node_group_role" {
  name = "eks-node-group-integ"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "integ-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.integ_node_group_role.name
}

resource "aws_iam_role_policy_attachment" "integ-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.integ_node_group_role.name
}

resource "aws_iam_role_policy_attachment" "integ-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.integ_node_group_role.name
}

resource "aws_eks_node_group" "integ_node_group" {
  cluster_name    = aws_eks_cluster.kube_cluster.name
  node_group_name = "integ_nodes"
  node_role_arn   = aws_iam_role.integ_node_group_role.arn
  subnet_ids      = aws_subnet.wespeakcloud[*].id
  instance_types  = ["t3.medium"]

  scaling_config {
    desired_size = 3
    max_size     = 10
    min_size     = 3
  }

  # Ensure that IAM Role permissions are created before and d
  #deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.integ-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.integ-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.integ-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_eks_node_group" "monitoring_node_group" {
  cluster_name    = aws_eks_cluster.kube_cluster.name
  node_group_name = "monitoring_nodes"
  node_role_arn   = aws_iam_role.integ_node_group_role.arn
  subnet_ids      = aws_subnet.wespeakcloud[*].id
  instance_types  = ["t3.large"]

  scaling_config {
    desired_size = 6
    max_size     = 10
    min_size     = 6
  }

  # Ensure that IAM Role permissions are created before and d
  #deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.integ-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.integ-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.integ-AmazonEC2ContainerRegistryReadOnly,
  ]
  labels = {
    "node_type" = "monitoring"
  }
}
