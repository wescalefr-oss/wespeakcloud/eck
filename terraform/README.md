# Prerequisites

Install direnv :
```bash
$ sudo apt install direnv
```

Unlock the .envrc file :
```bash
$ direnv allow
```

When your token expires :
```bash
$ source .envrc
```

# Initialize 

Initialize the Terraform backend module :

```bash
$ terraform init -backend-config=backend.tfvars
```

# Integration


Import Kube Config : 

    aws eks --region eu-west-1  update-kubeconfig --name wespeakcloud --profile wescale-dev

# Cluster configuration

## Nodes groups

* integ_nodes : t3.medium instances for deploying apps.
* monitoring_nodes : t3.large instances for deploying monitoring apps.
    The Kubernetes node label : node_type is set to 'monitoring'.

### Nodes groups selector

For choose which nodes for pods deployment add this nodeSelector specification into your K8S deploylment :
```yaml
spec:
  nodeSelector:
    node_type: monitoring
```
